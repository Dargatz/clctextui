﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLCTextUI
{
    public static class InterfaceUtility
    {
        public const string inputError = "Could not read input.";

        public const int WindowWidth = 80;
        public const int WindowHeight = 25;


        public static string getTitleLine(string title)
        {
            return getTitleLine(title, WindowWidth);
        }

        public static string getTitleLine(List<string> list)
        {
            if (list.Count == 0) return getTitleLine(WindowWidth);
            string temp = list[0];

            for(int i = 1; i < list.Count; i++)
            {
                temp += " - " + list[i];
            }
            return getTitleLine(temp);
        }
        public static string getTitleLine(int num)
        {
            string temp = "";
            for(int i = 0; i < num; i++)
            {
                temp += '#';
            }
            return temp;
        }

        public static string getTitleLine(string title, int length)
        {
            int hashTagLength = (WindowWidth - title.Length - 4)/2;
            string returnString = "";

            for(int i = 0; i < hashTagLength; i++)
            {
                returnString += '#';
            }

            returnString += "  " + title + "  ";

            for (int i = 0; i < hashTagLength; i++)
            {
                returnString += '#';
            }
            return returnString;
        }

        public static int CountLinesInString(string s)
        {
            int newLineLen = Environment.NewLine.Length;
            int numLines = s.Length - s.Replace(Environment.NewLine, string.Empty).Length;

            Console.WriteLine("len: " + newLineLen + "  num: " + numLines + " new: " + Environment.NewLine);

            numLines /= newLineLen;
            numLines++;
            return numLines;
        }

        public static string CreateTwoColumnedLine(int length, string one, string two)
        {
            string final = one;
            for (int i = final.Length; i < length/2; i++) { final += ' '; }
            final += two;
            for (int i = final.Length; i < length; i++) { final += ' '; }
            return final;
        }
    }
}
