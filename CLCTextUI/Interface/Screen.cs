﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CLCTextUI
{
    public enum ScreenCode
    {
        Normal,
        End,
        BadInput,
        ScrollUp,
        ScrollDown
    }

    public abstract class Screen
    {
        public const string backInput = "BACKSPACE123456789";
        public const string scrollUpInput = "PAGEUP123456789";
        public const string scrollDownInput = "PAGEDOWN123456789";

        public string Title { get; set; }


        protected Dictionary<string, Action> inputMap;

        protected List<string> content;
        protected int currentPage, numberOfPages, maxScreenLines;

        private bool hasTitleBar, hasBottomBar, ignoreCase;
        private bool backDisabled = false;

        public Screen(string title)
        {
            Title = title;
            inputMap = new Dictionary<string, Action>();
            hasTitleBar = true;
            hasBottomBar = true;
            ignoreCase = true;
        }

        #region Settings

        /// <summary>
        /// Prevents the backspace key from ending the screen execution.
        /// </summary>
        public void DisableBack()
        {
            backDisabled = true;
        }

        /// <summary>
        /// Set whether this screen displays a title line.
        /// </summary>
        /// <param name="newSetting"></param>
        public void SetTitleBar(bool newSetting)
        {
            hasTitleBar = newSetting;
        }

        /// <summary>
        /// Set whether this screen displays a bottom line with the current default options (backspacke, page up/down).
        /// </summary>
        /// <param name="newSetting"></param>
        public void SetBottomBar(bool newSetting)
        {
            hasBottomBar = newSetting;
        }

        /// <summary>
        /// Set whether text inputs ignore case or not. Ignores by default.
        /// </summary>
        /// <param name="newSetting"></param>
        public void SetIgnoreCase(bool newSetting)
        {
            ignoreCase = newSetting;
        }
        #endregion

        public virtual void AddOption(string inputVal, Action a)
        {
            if (ignoreCase) inputVal = inputVal.ToUpper();
            inputMap.Add(inputVal, a);
        }

        public void Execute()
        {
            ScreenCode returnVal = ScreenCode.Normal;
            while (returnVal != ScreenCode.End)
            {
                returnVal = Execute(returnVal);
            }
        }
        private ScreenCode Execute(ScreenCode val)
        {
            Console.SetWindowPosition(0, Console.CursorTop);
            
            if (val == ScreenCode.ScrollDown)
            {
                currentPage = Math.Min(currentPage + 1, numberOfPages);
            } else if (val == ScreenCode.ScrollUp)
            {
                currentPage = Math.Max(0, currentPage - 1);
            } else if (val == ScreenCode.BadInput) 
            {
                //Do nothing
            }
            else
            {
                content = new List<string>();
                currentPage = 0;
                CreateContent(val);
                EvaluateContentSize();
            }

            if (hasTitleBar) PrintTitleBar();
            PrintContent();
            if (hasBottomBar) PrintBottomBar();
            if (val == ScreenCode.BadInput) Console.WriteLine(InterfaceUtility.inputError);
            return ProcessInput();
        }



        #region Input Functions

        protected abstract bool isCustomInput(string input);
        protected abstract ScreenCode processCustomInput(string input);

        private ScreenCode ProcessInput()
        {

            string input = getInput();
            //Testing if input was an int

            if(ignoreCase)input = input.ToUpper();
            if (isDefaultInputString(input))
            {
                return processDefaultInput(input);
            }
            else if (inputMap.ContainsKey(input))
            {
                inputMap[input]();
                return ScreenCode.Normal;
            }
            else if (isCustomInput(input))
            {
                return processCustomInput(input);
            }
            else
            {
                return ScreenCode.BadInput;
            }
        }

        private string getInput()
        {
            string input = "";
            bool endInput = false;
            ConsoleKeyInfo pressed;

            while (!endInput)
            {
                pressed = Console.ReadKey();

                //Check if it is an automatic action input
                if (input.Length == 0 && isDefaultInputCode(pressed, out string defaultString)) return defaultString;
                if (pressed.Key == ConsoleKey.Enter) return input;
                if (pressed.Key == ConsoleKey.Backspace)
                {
                    Console.Write(" \b");
                    input = input.Substring(0, input.Length - 1);
                } else
                {
                    input += pressed.KeyChar;
                }
                
            }

            return "Should not be returning this";
        }

        private bool isDefaultInputCode(ConsoleKeyInfo pressed, out string val)
        {
            val = "";
            if (pressed.Key == ConsoleKey.PageDown) val = scrollDownInput;
            if (pressed.Key == ConsoleKey.PageUp) val = scrollUpInput;
            if (pressed.Key == ConsoleKey.Backspace) val = backInput;

            if (val.Length == 0) return false;
            return true;
        }

        private bool isDefaultInputString(string pressed)
        {
            if (pressed == scrollDownInput) return true;
            if (pressed == scrollUpInput) return true;
            if (pressed == backInput) return true;
            return false;
        }

        private ScreenCode processDefaultInput(string input)
        {
            switch (input)
            {
                case scrollDownInput:
                    return ScreenCode.ScrollDown;
                case scrollUpInput:
                    return ScreenCode.ScrollUp;
                case backInput:
                    if (backDisabled) return ScreenCode.Normal;
                    return ScreenCode.End;
                default:
                    return ScreenCode.BadInput;
            }
        }
        #endregion

        #region Display Functions

        protected abstract void CreateContent(ScreenCode val);

        private void PrintContent()
        {
            for (int i = currentPage * maxScreenLines; i < (currentPage * maxScreenLines) + maxScreenLines; i++)
            {
                if(i >= content.Count)
                {
                    //Console.WriteLine("");
                } else
                {
                    Console.WriteLine(content[i]);
                }
                
            }
        }

        private void EvaluateContentSize()
        {
            maxScreenLines = calcMaxScreenLines();

            numberOfPages = (content.Count / maxScreenLines);
        }

        protected void PrintTitleBar()
        {
            string pageNumber = "";
            if(numberOfPages > 0)
            {
                pageNumber = "(" + (currentPage + 1) + "/" + (numberOfPages + 1) + ")";
            }
            Console.WriteLine(InterfaceUtility.getTitleLine(Title + pageNumber));
            Console.WriteLine("");
        }

        protected void PrintBottomBar()
        {
            Console.WriteLine("");

            var options = new List<string>();
            if (!backDisabled) options.Add("Backspace");
            if (!(currentPage == numberOfPages)) options.Add("Page Down");
            if (currentPage != 0) options.Add("Page Up");

            Console.WriteLine(InterfaceUtility.getTitleLine(options));
        }

        public void addLineToContent(string toAdd)
        {
            content.Add(toAdd);
        }

        #endregion

        #region Formatting

        private int calcMaxScreenLines()
        {
            int temp = InterfaceUtility.WindowHeight;
            if (hasBottomBar) temp -= 2;
            if (hasTitleBar) temp -= 2;

            return temp;
        }

        /// <summary>
        /// Used to add a line to the content of a screen
        /// </summary>
        /// <param name="toAdd"></param>
        public void addIndentedLineToContent(string toAdd)
        {
            addLineToContent("     " + toAdd);
        }

        /// <summary>
        /// Adds a BoxedSection to the content of a screen.
        /// </summary>
        /// <param name="section"></param>
        public void addSectionToContent(BoxedSection section)
        {
            addLineToContent("");

            
            addLineToContent(section.AsString);
        }
        #endregion
    }
}
