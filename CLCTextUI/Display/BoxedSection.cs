﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLCTextUI
{
    public class BoxedSection
    {
        private string title;
        private string option;
        private Action optionAction;
        private List<string> content;

        public BoxedSection(string t, string o = null, Action oA = null)
        {
            title = t;
            option = o;
            optionAction = oA;
            content = new List<string>();
        }

        public void AddLine(string toAdd)
        {
            content.Add(toAdd);
        }


        public int InnerWidth { get { return InterfaceUtility.WindowWidth - 4; } }
        public string AsString 
        {
            get
            {
                string final = "";
                final += "┌─" + title;

                int limit = InterfaceUtility.WindowWidth;
                if (option != null) limit -= 3 + option.Length;
                for (int i = final.Length; i < limit; i++) { final += '─'; }
                if (option != null) final += "(" + option + ")";
                final += '┐';

                foreach(string lin in content)
                {
                    final += Environment.NewLine;
                    final += "│ " + lin;
                    for (int i = 2 + lin.Length; i < InterfaceUtility.WindowWidth - 2; i++) { final += ' '; }
                    final += " │";
                }

                final += Environment.NewLine;
                final += '└';
                for (int i = 1; i < InterfaceUtility.WindowWidth - 1; i++) { final += '─'; }
                final += '┘';
                return final;
            }
        }
    }
}
