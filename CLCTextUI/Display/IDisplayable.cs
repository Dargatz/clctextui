﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLCTextUI
{
    public interface IDisplayable
    {
        string Title { get;}
        void CreateContent(ObjectDisplay display);
        void Display();
    }
}
