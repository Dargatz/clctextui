﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLCTextUI
{ 
    public class ObjectDisplay : Screen
    {
        private IDisplayable objectToDisplay;

        public ObjectDisplay(IDisplayable oToD) : base(oToD.Title)
        {
            objectToDisplay = oToD;
        }

        protected override void CreateContent(ScreenCode val)
        {
            objectToDisplay.CreateContent(this);
        }

        protected override bool isCustomInput(string input)
        {
            return false;
        }

        protected override ScreenCode processCustomInput(string input)
        {
            return ScreenCode.BadInput;
        }
    }
}
