﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace CLCTextUI
{
    public class StringInput : Screen
    {
        private string message, finalInput;
        private Func<string, bool> tester;
        public StringInput(string m, Func<string, bool> t = null) : base("")
        {
            message = m;
            tester = t;
            this.SetTitleBar(false);
            this.SetBottomBar(false);
            this.SetIgnoreCase(false);
        }

        public string GetInput()
        {
            finalInput = null;

            this.Execute();

            return finalInput;
        }

        protected override void CreateContent(ScreenCode val)
        {
            addLineToContent(message);
        }

        protected override bool isCustomInput(string input)
        {
            if (input.Any(char.IsControl)) return false;
            if (tester != null && !tester(input)) return false;
            return true;
        }

        protected override ScreenCode processCustomInput(string input)
        {
            finalInput = input;
            return ScreenCode.End;
        }
    }
}
