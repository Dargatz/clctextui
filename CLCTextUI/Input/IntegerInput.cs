﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace CLCTextUI
{
    public class IntegerInput : Screen
    {
        private string message;
        private Func<int, bool> tester;
        private int? finalInput;
        public IntegerInput(string m, Func<int, bool> t = null) : base("")
        {
            message = m;
            tester = t;
            this.SetTitleBar(false);
            this.SetBottomBar(false);
        }

        public int? GetInput()
        {
            finalInput = null;

            this.Execute();

            return finalInput;
        }

        protected override void CreateContent(ScreenCode val)
        {
            addLineToContent(message);
        }

        protected override bool isCustomInput(string input)
        {
            if (input.Any(char.IsControl)) return false;
            if (!Int32.TryParse(input, out int intInput)) return false;
            if (tester != null && !tester(intInput)) return false;
            return true;
        }

        protected override ScreenCode processCustomInput(string input)
        {
            finalInput = Int32.Parse(input);
            return ScreenCode.End;
        }

        private bool GetInput(out int val)
        {
            Console.SetWindowPosition(0, Console.CursorTop);

            Console.WriteLine(message);
            string input = Console.ReadLine();

            if (Int32.TryParse(input, out val))
            {
                if (tester(val)) return true;
            }
            return false;
        }

    }
}
