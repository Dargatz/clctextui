﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLCTextUI
{
    public class TextMenu : Screen
    {

        private List<string> optionStrings;
        private int numberOfOptions;
        private Action<int> integerInputFunction;

        public TextMenu(string tempTitle, Action<int> con = null) : base(tempTitle)
        {
            optionStrings = new List<string>();
            integerInputFunction = con;
            numberOfOptions = 0;
        }


        /// <summary>
        /// Adds a numbered option to the menu. When selected, the a action parameter will be called.
        /// </summary>
        /// <param name="textToDisplay"></param>
        /// <param name="a"></param>
        public override void AddOption(string textToDisplay, Action a)
        {
            base.AddOption("" + (numberOfOptions + 1), a);
            add(textToDisplay);
        }


        /// <summary>
        /// Adds a numbered option to the menu. When selected, the integerInputFunction action will be called with the option number as input.
        /// </summary>
        /// <param name="textToDisplay"></param>
        public void AddOption(string textToDisplay)
        {
            add(textToDisplay);
        }

        private void add(string t)
        {
            optionStrings.Add(t);
            numberOfOptions++;
        }

        protected override void CreateContent(ScreenCode val)
        {
            for (int i = 0; i < optionStrings.Count; i++)
            {
                addIndentedLineToContent(CreateNumberedOptionString(i + 1, optionStrings[i]));
            }
        }

        protected override bool isCustomInput(string input)
        {
            return Int32.TryParse(input, out int inputInt) && inputInt <= numberOfOptions && inputInt > 0 && integerInputFunction != null;
        }

        protected override ScreenCode processCustomInput(string input)
        {
            integerInputFunction(Int32.Parse(input));
            return ScreenCode.Normal;
        }

        #region Formatting

        private string CreateNumberedOptionString(int num, string option)
        {
            return "" + num + ".) " + option;
        }

        #endregion
    }
}
