﻿using System;
using CLCTextUI;

namespace ExampleProject
{
    class Program
    {
        static DataClass data;
        static void Main(string[] args)
        {
            data = new DataClass();
            CreateMainMenu();
        }

        public static void CreateMainMenu()
        {
            var menu = new TextMenu("Main Menu");
            menu.DisableBack();

            menu.AddOption("View Data", data.Display);
            menu.AddOption("Change Example String", data.ChangeExampleString);
            menu.AddOption("Quit", Quit);

            menu.Execute();
        }

        public static void Quit()
        {
            Environment.Exit(0);
        }
    }
}
