﻿using System;
using System.Collections.Generic;
using System.Text;
using CLCTextUI;

namespace ExampleProject
{
    public class DataClass : IDisplayable
    {
        

        private string ExampleString;

        public DataClass()
        {
            Title = "Data";
            ExampleString = "Defaul Value";
        }

        public void ChangeExampleString()
        {
            var input = new StringInput("Set new Example String. Must be between 1 and 15 characters.", ExampleStringTester);

            setExampleString(input.GetInput());
        }

        public bool ExampleStringTester(string input)
        {
            if (input.Length < 1 || input.Length > 15) return false;
            return true;
        }


        private void setExampleString(string newString)
        {
            ExampleString = newString ?? ExampleString;
        }

        #region IDisplayable 
        public string Title { get; set; }

        public void CreateContent(ObjectDisplay display)
        {
            display.addLineToContent("Example String: " + ExampleString);
        }

        public void Display()
        {
            var displayer = new ObjectDisplay(this);
            displayer.Execute();
        }
        #endregion
    }
}
